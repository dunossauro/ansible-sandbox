from flask import Flask, request

app = Flask(__name__)

def valida_formulário(form):
    if form:
        if isinstance(form, dict):
            if 'Nome' in form:
                return True
    return False


@app.route('/')
def hello():
    return 'Olá mundo'


@app.route('/formulario', methods=['POST'])
def formulário():
    if valida_formulário(request.json):
        return 'Aceito'
    return 'Inválido'
