from behave import when, then
from requests import get

@when(u'acessar o root')
def acesso_root(context):
    context.result = get('http://localhost:8080/').text


@then(u'devo receber a mensagem')
def step_impl(context):
    assert context.result == context.text
