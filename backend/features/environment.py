from ipdb import spost_mortem

def after_step(context, step):
    if step.status == 'failed':
        spost_mortem(step.exc_traceback)
