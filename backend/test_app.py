from unittest import TestCase, main, mock
from app import app, hello, formulário, valida_formulário
from flask import url_for

class TestRoot(TestCase):
    def setUp(self):
        self.app = app
        self.app.config['SERVER_NAME'] = 'localhost:5000'
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.app.testing = True
        self.client = self.app.test_client()

    def test_rota_root_retorna_200_com_get(self):
        response = self.client.get(url_for('hello'))

        self.assertEqual(response.status_code, 200)

    def test_rota_root_retorna_ola_mundo_no_corpo_da_mensagem_com_get(self):
        response = self.client.get(url_for('hello'))

        self.assertEqual(response.data.decode(), 'Olá mundo')


class TestHello(TestCase):
    def test_hello_retorna_ola_mundo(self):
        esperado = 'Olá mundo'
        recebido = hello()

        self.assertEqual(esperado, recebido)

class TestRotaFormulário(TestCase):
    def setUp(self):
        self.app = app
        self.app.config['SERVER_NAME'] = 'localhost:5000'
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.app.testing = True
        self.client = self.app.test_client()

    def test_formulário_retorna_200_quando_enviado(self):
        response = self.client.post(url_for('formulário'), json={})

        self.assertEqual(response.status_code, 200)

    def test_formulário_retorna_aceito_quando_formulário_for_enviado(self):
        response = self.client.post(
            url_for('formulário'),
            json={'Nome': 'Vicente'}
        )

        self.assertEqual(response.data.decode(), 'Aceito')

    def test_retorna_invalido_quando_formulario_estiver_invalido(self):
        response = self.client.post(
            url_for('formulário'),
            json={}
        )
        self.assertEqual(response.data.decode(), 'Inválido')


class TestFormulário(TestCase):

    @mock.patch('app.request', return_value=True)
    @mock.patch('app.valida_formulário', return_value=True)
    def test_formulário_retorna_aceito(self, m_valida, m_request):
        resposta = formulário()

        self.assertEqual(resposta, 'Aceito')

    @mock.patch('app.valida_formulário', return_value=False)
    @mock.patch('app.request', return_value=True)
    def test_formulário_retorna_invalido(self, m_request, m_valida):
        resposta = formulário()

        self.assertEqual(resposta, 'Inválido')


class TestValidaFormulário(TestCase):
    def test_valida_formulário_quando_receber_json_válido(self):
        esperado = True
        resposta = valida_formulário({'Nome': 'João'})

        self.assertEqual(resposta, esperado)

    def test_valida_formulário_quando_receber_json_inválido(self):
        esperado = False
        resposta = valida_formulário('Olá bb, qual seu Nome')

        self.assertEqual(resposta, esperado)


if __name__ == '__main__':
    main()
